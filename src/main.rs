use std::cmp;
use std::fs;
use std::io::{self, BufRead};
use std::path::Path;

fn main() -> Result<(), std::io::Error> {
  let readings = data_from_report_file("./res/input")?;
  println!("Part One");
  let cmps = cmps_from_data(&readings);
  let [gt, eq, lt] = count_cmps(&cmps);
  println!(
    "increasing readings: {}\n\
    equivalent readings: {}\n\
    decreasing readings: {}\n",
    gt, eq, lt);

  println!("Part Two");
  let sliding_sums = sliding_3_window(&readings).iter()
    .map(|&t| t.iter().sum())
    .collect::<Vec<isize>>();
  let sliding_cmps = cmps_from_data(&sliding_sums);
  let [sliding_gt, sliding_eq, sliding_lt] = count_cmps(&sliding_cmps);
  println!(
    "sliding window increasing readings: {}\n\
    sliding window equivalent readings: {}\n\
    sliding window decreasing readings: {}\n",
    sliding_gt, sliding_eq, sliding_lt);
  Ok(())
}

fn data_from_report_file<P: AsRef<Path>>(path: P) -> Result<Vec<isize>, std::io::Error> {
  let file = fs::File::open(path)?;
  let result = io::BufReader::new(file)
    .lines()
    .map(|line| line.unwrap().parse::<isize>().unwrap())
    .collect();
  Ok(result)
}

fn cmps_from_data(data: &Vec<isize>) -> Vec<cmp::Ordering> {
  data
    .iter()
    .zip(data.iter().skip(1))
    .map(|(reading, next_reading)| next_reading.cmp(reading))
    .collect()
}

fn count_cmps(cmps: &Vec<cmp::Ordering>) -> [usize; 3] {
  [
    cmps
      .iter()
      .filter(|c| c.is_gt())
      .count(),
    cmps
      .iter()
      .filter(|c| c.is_eq())
      .count(),
    cmps
      .iter()
      .filter(|c| c.is_lt())
      .count(),
  ]
}

fn sliding_3_window(data: &Vec<isize>) -> Vec<[isize; 3]>{
  data.iter()
    .zip(data.iter().skip(1))
    .zip(data.iter().skip(2))
    .map(|((first, second), third)| [*first, *second, *third])
    .collect()
}

